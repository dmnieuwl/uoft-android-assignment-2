package com.tultechip.vanityapp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlHelper {

    private static final Pattern linkPattern = Pattern.compile("\\<[^\\>]*\\>");
    private static final Pattern htmlEncodingPattern = Pattern.compile("&#(\\d+);");

    public static String filterHtmlText(String text) {
        if (text == null) {
            return null;
        }

        // Remove all of the embedded html
        String filteredText = linkPattern.matcher(text).replaceAll("");

        // Replace all of the encoded html characters
        Matcher matcher = htmlEncodingPattern.matcher(filteredText );
        StringBuffer stringBuffer = new StringBuffer(filteredText.length());
        while (matcher.find()) {
            String code = matcher.group(1);
            switch(code) {
                case "8211":
                    code = "–";
                    break;
                case "8217":
                    code = "’";
                    break;
                case "8220":
                    code = "“";
                    break;
                case "8221":
                    code = "”";
                    break;
                case "8230":
                    code = "…";
                    break;
                default:
                    break;
            }
            matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(code));
        }
        matcher.appendTail(stringBuffer);
        filteredText = stringBuffer.toString();

        filteredText = filteredText.replaceAll("\n+", "\n\n");

        filteredText = filteredText.trim();
        return filteredText ;
    }
}
