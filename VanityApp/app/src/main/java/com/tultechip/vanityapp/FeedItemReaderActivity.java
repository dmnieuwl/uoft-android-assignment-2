package com.tultechip.vanityapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * The activity responsible for coordinating the data into the feed item reader.
 */
public class FeedItemReaderActivity extends AppCompatActivity {

    public static final String EXTRA_CONTENT = "com.tultechip.vanityapp.EXTRA_CONTENT";
    public static final String EXTRA_DATE = "com.tultechip.vanityapp.EXTRA_DATE";
    public static final String EXTRA_TITLE = "com.tultechip.vanityapp.EXTRA_TITLE";

    TextView contentTextView;
    TextView dateTextView;
    ImageView headerImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_item_reader);

        contentTextView = findViewById(R.id.content);
        dateTextView = findViewById(R.id.date);
        headerImageView = findViewById(R.id.header_image);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();
        String title = intent.getStringExtra(EXTRA_TITLE);
        if (title != null) {
            setTitle(title);
        }

        String date = intent.getStringExtra(EXTRA_DATE);
        if (date != null) {
            dateTextView.setText(date);
        }

        String content = intent.getStringExtra(EXTRA_CONTENT);
        if (content != null) {
            contentTextView.setText(content);
        }

        int[] headerIds = {
                R.drawable.article_header_1,
                R.drawable.article_header_2,
                R.drawable.article_header_3
        };
        headerImageView.setImageResource(headerIds[content.length() % headerIds.length]);
    }
}
