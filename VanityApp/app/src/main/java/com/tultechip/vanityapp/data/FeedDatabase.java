package com.tultechip.vanityapp.data;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.Date;

@Database(entities = FeedItem.class, version = 1, exportSchema = false)
@TypeConverters({DateTypeConverter.class, CategoriesTypeConverter.class})
public abstract class FeedDatabase extends RoomDatabase {

    private static FeedDatabase instance;

    public abstract FeedItemDao feedItemDao();

    public static synchronized FeedDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    FeedDatabase.class,
                    "feed_database").fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };
}
