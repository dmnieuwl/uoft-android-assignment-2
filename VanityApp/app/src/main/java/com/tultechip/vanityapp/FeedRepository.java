package com.tultechip.vanityapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.prof.rssparser.Article;
import com.prof.rssparser.OnTaskCompleted;
import com.prof.rssparser.Parser;
import com.tultechip.vanityapp.data.FeedDatabase;
import com.tultechip.vanityapp.data.FeedItem;
import com.tultechip.vanityapp.data.FeedItemDao;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class FeedRepository {

    // Private members

    private FeedItemDao feedItemDao;
    private LiveData<List<FeedItem>> allFeedItems;

    public FeedRepository(Application application) {
        // Initialize the database
        FeedDatabase database = FeedDatabase.getInstance(application);
        feedItemDao = database.feedItemDao();
        allFeedItems = feedItemDao.getAllFeedsItems();

        // Initialize the network
        fetchAllFeedItems();
    }

    // Public methods

    public LiveData<List<FeedItem>> getAllFeedItems() {
        return allFeedItems;
    }

    public void delete(FeedItem feedItem) {
        new DeleteFeedItemAsyncTask(feedItemDao).execute(feedItem);
    }

    public void deleteAll() {
        new DeleteAllFeedItemAsyncTask(feedItemDao).execute();
    }

    public void fetchAllFeedItems() {
        Parser parser = new Parser();
        parser.onFinish(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted(@NotNull List<Article> articles) {
                for (Article article : articles) {

                    Date pubDate = null;
                    String dateString = article.getPubDate();
                    if (dateString != null) {
                        try {
                            DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
                            pubDate = formatter.parse(dateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    FeedItem feedItem = new FeedItem(
                            article.getGuid(),
                            pubDate,
                            article.getTitle(),
                            article.getLink(),
                            article.getAuthor(),
                            HtmlHelper.filterHtmlText(article.getDescription()),
                            HtmlHelper.filterHtmlText(article.getContent()),
                            article.getImage(),
                            article.getCategories());
                    insertOrUpdate(feedItem);
                }
            }

            @Override
            public void onError(@NotNull Exception e) {
            }
        });
        parser.execute("https://nieuwland.ca/feed/");
    }

    public void insertOrUpdate(FeedItem feedItem) {
        new InsertOrUpdateFeedItemAsyncTask(feedItemDao).execute(feedItem);
    }

    public void update(FeedItem feedItem) {
        new UpdateFeedItemAsyncTask(feedItemDao).execute(feedItem);
    }

    // Private DAO members

    private static class DeleteFeedItemAsyncTask extends AsyncTask<FeedItem, Void, Void> {
        private FeedItemDao feedItemDao;

        private DeleteFeedItemAsyncTask(FeedItemDao feedItemDao) {
            this.feedItemDao = feedItemDao;
        }

        @Override
        protected Void doInBackground(FeedItem... feedItems) {
            feedItemDao.delete(feedItems[0]);
            return null;
        }
    }

    private static class DeleteAllFeedItemAsyncTask extends AsyncTask<FeedItem, Void, Void> {
        private FeedItemDao feedItemDao;

        private DeleteAllFeedItemAsyncTask(FeedItemDao feedItemDao) {
            this.feedItemDao = feedItemDao;
        }

        @Override
        protected Void doInBackground(FeedItem... feedItems) {
            feedItemDao.deleteAllFeedItems();
            return null;
        }
    }

    private static class InsertOrUpdateFeedItemAsyncTask extends AsyncTask<FeedItem, Void, Void> {
        private FeedItemDao feedItemDao;

        private InsertOrUpdateFeedItemAsyncTask(FeedItemDao feedItemDao) {
            this.feedItemDao = feedItemDao;
        }

        @Override
        protected Void doInBackground(FeedItem... feedItems) {
            FeedItem feedItem = feedItems[0];
            List<FeedItem> existingFeedItems = feedItemDao.getItemByGuid(feedItem.getGuid());
            if (existingFeedItems == null || existingFeedItems.isEmpty()) {
                feedItemDao.insert(feedItem);
            } else {
                feedItemDao.update(feedItem);
            }
            return null;
        }
    }

    private static class UpdateFeedItemAsyncTask extends AsyncTask<FeedItem, Void, Void> {
        private FeedItemDao feedItemDao;

        private UpdateFeedItemAsyncTask(FeedItemDao feedItemDao) {
            this.feedItemDao = feedItemDao;
        }

        @Override
        protected Void doInBackground(FeedItem... feedItems) {
            feedItemDao.update(feedItems[0]);
            return null;
        }
    }
}
