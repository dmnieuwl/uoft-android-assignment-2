package com.tultechip.vanityapp.data;

import androidx.room.TypeConverter;

import java.util.Arrays;
import java.util.List;

public class CategoriesTypeConverter {
    @TypeConverter
    public static List<String> toStringList(String text) {
        if (text == null || text.length() == 0) {
            return null;
        }
        return Arrays.asList(text.split(","));
    }

    @TypeConverter
    public static String toString(List<String> stringList) {
        if (stringList == null || stringList.size() == 0) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String element : stringList) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(",");
            }
            stringBuilder.append(element);
        }
        return stringBuilder.toString();
    }
}
