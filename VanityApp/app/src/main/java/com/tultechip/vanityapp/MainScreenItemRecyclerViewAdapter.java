package com.tultechip.vanityapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.tultechip.vanityapp.data.FeedItem;

import java.util.List;

public class MainScreenItemRecyclerViewAdapter extends ListAdapter<FeedItem, MainScreenItemRecyclerViewAdapter.MainScreenItemViewHolder> {

    private static final DiffUtil.ItemCallback<FeedItem> DIFF_CALLBACK = new DiffUtil.ItemCallback<FeedItem>() {
        @Override
        public boolean areItemsTheSame(@NonNull FeedItem oldItem, @NonNull FeedItem newItem) {
            return oldItem.getGuid().equals(newItem.getGuid());
        }

        @Override
        public boolean areContentsTheSame(@NonNull FeedItem oldItem, @NonNull FeedItem newItem) {
            String oldItemCategory = oldItem.getCategories().get(0);
            String newItemCategory = newItem.getCategories().get(0);
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getBlurb().equals(newItem.getBlurb()) &&
                    oldItemCategory.equals(newItemCategory);
        }
    };

    OnItemClickListener listener;

    public MainScreenItemRecyclerViewAdapter() {
        super(DIFF_CALLBACK);
    }

    public FeedItem getFeedItemAt(int position) {
        return getItem(position);
    }

    @NonNull
    @Override
    public MainScreenItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_screen_item, parent, false);
        return new MainScreenItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainScreenItemViewHolder holder, int position) {
        FeedItem currentItem = getItem(position);

        List<String> categories = currentItem.getCategories();
        if (categories == null || categories.size() == 0) {
            holder.titleTextView.setText("Article");
        } else {
            holder.titleTextView.setText(categories.get(0));
        }
        holder.subtitleTextView.setText(currentItem.getTitle());
        holder.contentTextView.setText(currentItem.getBlurb());
    }

    @Override
    public int getItemCount() {
        return Math.min(super.getItemCount(), 3);
    }

    public void setOnItemClientListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(FeedItem feedItem);
    }

    public class MainScreenItemViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextView;
        public TextView subtitleTextView;
        public TextView contentTextView;

        public MainScreenItemViewHolder(@NonNull View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.title);
            subtitleTextView = itemView.findViewById(R.id.subtitle);
            contentTextView = itemView.findViewById(R.id.content);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(getItem(position));
                    }
                }
            });
        }
    }
}
