package com.tultechip.vanityapp.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface FeedItemDao {

    @Insert
    void insert(FeedItem feedItem);

    @Query("SELECT * FROM feed_table WHERE guid = :guid")
    List<FeedItem> getItemByGuid (@NonNull String guid);

    @Update
    void update(FeedItem feedItem);

    @Delete
    void delete(FeedItem feedItem);

    @Query("DELETE FROM feed_table")
    void deleteAllFeedItems();

    @Query("SELECT * FROM feed_table ORDER BY publicationDate DESC")
    LiveData<List<FeedItem>> getAllFeedsItems();
}
