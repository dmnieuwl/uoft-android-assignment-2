package com.tultechip.vanityapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.tultechip.vanityapp.data.FeedItem;

import java.util.List;

public class FeedItemViewModel extends AndroidViewModel {

    private FeedRepository repository;
    private LiveData<List<FeedItem>> allFeedItems;

    public FeedItemViewModel(@NonNull Application application) {
        super(application);

        repository = new FeedRepository(application);
        allFeedItems = repository.getAllFeedItems();
    }

    public LiveData<List<FeedItem>> getAllFeedItems() {
        return allFeedItems;
    }
}
