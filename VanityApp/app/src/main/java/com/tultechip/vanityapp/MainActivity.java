package com.tultechip.vanityapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tultechip.vanityapp.data.FeedItem;

import java.text.DateFormat;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration decorator = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        decorator.setDrawable(ContextCompat.getDrawable(recyclerView.getContext(), R.drawable.divider));
        recyclerView.addItemDecoration(decorator);

        // Set up the adapter
        final MainScreenItemRecyclerViewAdapter recyclerViewAdapter = new MainScreenItemRecyclerViewAdapter();
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.setOnItemClientListener(new MainScreenItemRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(FeedItem feedItem) {
                Intent intent = new Intent(MainActivity.this, FeedItemReaderActivity.class);
                intent.putExtra(FeedItemReaderActivity.EXTRA_TITLE, feedItem.getTitle());
                intent.putExtra(FeedItemReaderActivity.EXTRA_DATE, DateFormat.getDateInstance(DateFormat.MEDIUM).format(feedItem.getPublicationDate()));
                intent.putExtra(FeedItemReaderActivity.EXTRA_CONTENT, feedItem.getContent());
                startActivity(intent);
            }
        });

        FeedItemViewModel feedItemViewModel = ViewModelProviders.of(this).get(FeedItemViewModel.class);
        feedItemViewModel.getAllFeedItems().observe(this, new Observer<List<FeedItem>>() {
            @Override
            public void onChanged(List<FeedItem> feedItems) {
                recyclerViewAdapter.submitList(feedItems);
            }
        });
    }
}
