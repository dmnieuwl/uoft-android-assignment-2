package com.tultechip.vanityapp.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.List;

@Entity(tableName = "feed_table")
public class FeedItem {

    @PrimaryKey
    private @NonNull String guid;
    private Date publicationDate;
    private String title;
    private String link;
    private String author;
    private String blurb;
    private String content;
    private String imageUrl;
    private List<String> categories;

    public FeedItem(@NonNull String guid, Date publicationDate, String title, String link, String author, String blurb, String content, String imageUrl, List<String> categories) {
        this.guid = guid;
        this.publicationDate = publicationDate;
        this.title = title;
        this.link = link;
        this.author = author;
        this.blurb = blurb;
        this.content = content;
        this.imageUrl = imageUrl;
        this.categories = categories;
    }

    // Getter Methods

    public String getGuid() {
        return guid;
    }

    public Date getPublicationDate() {
        return this.publicationDate;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getAuthor() {
        return author;
    }

    public String getBlurb() { return blurb; }

    public String getContent() {
        return content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public List<String> getCategories() {
        return categories;
    }
}